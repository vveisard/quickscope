using System;

namespace SINeDUSTRIES.QuickScope
{
  public abstract class ARun<TOutput> : IRun<TOutput>
  {
    #region Properties

    public RunState State { get; private set; } = RunState.Waiting;

    #endregion

    #region INode / methods

    /// <summary>
    /// Implements <see cref="IRun.Start"/>.
    /// </summary>
    public Boolean Run(out TOutput output)
    {
      if (this.State == RunState.Running)
      {
        //UnityEngine.Debug.Log($"Pipline / {this.GetType().Name}: Run");

        if (this.run(out output))
        {
          this.finish();

          this.State = RunState.Finished;

          return true;
        }

        return false;
      }
      else
      {
        throw new InvalidOperationException($"Node not Running: {this.State}.");
      }
    }

    /// <summary>
    /// Implements <see cref="IRun.Start"/>
    /// </summary>
    public void Start()
    {
      if (this.State == RunState.Waiting)
      {
        //UnityEngine.Debug.Log($"Pipline / {this.GetType().Name}: Start");

        this.start();

        this.State = RunState.Running;
      }
      else
      {
        throw new InvalidOperationException();
      }
    }

    #endregion

    #region this / methods / abstract

    abstract protected void start();

    abstract protected void finish();

    abstract protected Boolean run(out TOutput output);

    #endregion
  }
}