namespace SINeDUSTRIES.QuickScope
{
  public enum RunState
  {
    Waiting = 0,

    // Awake = 1,

    Running = 2,

    //Paused = 3,

    //Aborted = 4,

    Finished = 5
  }
}