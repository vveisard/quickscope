using System;

namespace SINeDUSTRIES.QuickScope
{
  public interface IConfigTextures
  {
    #region properties

    /// <summary>
    /// Antialiasing for RenderTexture.
    /// </summary>
    Int32 AntiAliasing { get; }

    #endregion
  }
}