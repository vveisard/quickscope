using System;

namespace SINeDUSTRIES.QuickScope
{
  public interface IConfigView
  {
    #region properties

    String name { get; }

    /// <summary>
    /// Distnace, on the x/z plane, to take shots from.  
    /// </summary>
    Single DistanceXZ { get; }

    /// <summary>
    /// Height, on the y axis, to take shots from.
    /// </summary>
    Single HeightY { get; }

    /// <summary>
    /// Angle, (yaw, rotation around the y axis), to start taking shots from.
    /// </summary>
    Single AngleYStart { get; }

    /// <summary>
    /// Number of angles to rotate.
    /// </summary>
    Int32 AngleYCuts { get; }

    #endregion
  }
}