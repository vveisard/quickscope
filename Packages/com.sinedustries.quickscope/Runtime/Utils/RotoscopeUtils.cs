using System;

namespace SINeDUSTRIES.QuickScope
{
  static public class RotoscopeUtils
  {
    /// <summary>
    /// Generate a filename from a <see cref="Sample"/>.
    /// </summary>
    static public String Filename(this Sample thisRotoscope)
    => $"{thisRotoscope.TargetName}-{thisRotoscope.ClipName}-{thisRotoscope.ViewName}-{thisRotoscope.ViewAngleIndex}-{thisRotoscope.ClipSampleIndex}";
    //=> $"{thisRotoscope.TargetName}-{thisRotoscope.ViewName}-{thisRotoscope.ViewAngleIndex}";
  }
}