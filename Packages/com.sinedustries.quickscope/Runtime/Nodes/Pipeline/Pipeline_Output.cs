namespace SINeDUSTRIES.QuickScope.Nodes
{
  public struct Pipeline_Output
  {
    Write_Output Write_Output { get; }

    public Pipeline_Output(Write_Output write_Output)
    {
      this.Write_Output = write_Output;
    }
  }
}